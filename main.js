const Company = (function () {
  const listeners = Symbol('listeners')
  const users = Symbol('users')
  const password = Symbol('password')
  const token = Symbol('token')

  class Company {
    constructor(name, usersLimit) {
      this.name = name
      this.usersLimit = usersLimit
      this[users] = []
      this[listeners] = {createUserListeners: [], updateUserListeners: [], noSpaceNotifierListeners: []}
    }

    static createAdmin(company) {
      const admin = new User("admin", "ADMIN", true)
      admin.company = company

      if (company[users].length) {
        throw new Error('Admin has been added')
      }
      company[users].push(admin)

      return admin
    }

    get usersCount() {
      return this[users].length
    }

    getUser(id) {
      const user = this[users].find(user => user.id === id)

      if (!user) {
        throw new Error('User not found')
      }

      return user
    }

    registerUserCreateCallback(...cb) {
      const createUserListeners = this[listeners].createUserListeners
      createUserListeners.push(...cb)
    }

    registerUserUpdateCallback(...cb) {
      const updateUserListeners = this[listeners].updateUserListeners
      updateUserListeners.push(...cb)
    }

    registerNoSpaceNotifier(...cb) {
      const noSpaceNotifierListeners = this[listeners].noSpaceNotifierListeners
      noSpaceNotifierListeners.push(...cb)
    }
  }

  class User {
    #id
    #name
    #lastName
    #role

    constructor(name, lastName, isAdmin = false) {
      this.#id = +new Date
      this.#name = name
      this.#lastName = lastName

      if (isAdmin) {
        this[token] = "secret-token"
        this[password] = prompt('Add password')
        this.createUser = createUser
        this.deleteUser = deleteUser
      }
    }

    get id() {
      return this.#id
    }

    get name() {
      return this.#name
    }

    set name(name) {
      this.#name = name
      const updateUserListeners = this[listeners].updateUserListeners
      updateUserListeners.forEach(cb => cb(this))
    }

    get lastName() {
      return this.#lastName
    }

    set lastName(lastName) {
      this.#lastName = lastName
      const updateUserListeners = this[listeners].updateUserListeners
      updateUserListeners.forEach(cb => cb(this))
    }

    get role() {
      return this.#role
    }

    set role(role) {
      this.#role = role
      const updateUserListeners = this[listeners].updateUserListeners
      updateUserListeners.forEach(cb => cb(this))
    }
  }

  function createUser(name, lastName) {
    checkPermission.call(this)
    const companyUsers = this.company[users]
    const companyCallbacks = this.company[listeners]
    const user = new User(name, lastName)

    user[listeners] = this.company[listeners]
    companyUsers.push(user)
    companyCallbacks.createUserListeners.forEach(cb => cb(user))

    if (this.company.usersLimit === companyUsers.length) {
      companyCallbacks.noSpaceNotifierListeners.forEach(cb => cb('The company is full'))
    }

    if (this.company.usersLimit < companyUsers.length) {
      throw new Error('There is no spaces')
    }

    return user
  }

  function deleteUser(id) {
    checkPermission.call(this)
    const companyCallbacks = this.company[listeners]
    const companyUsers = this.company[users]

    if (!companyUsers.find(user => user.id === id)) {
      throw new Error('User not found')
    }

    this.company[users] = companyUsers.filter(user => user.id !== id)
    companyCallbacks.updateUserListeners.forEach(cb => cb(id))
  }

  function checkPermission() {
    if (this[token] !== "secret-token") {
      throw new Error("Permission denied!")
    }
    if (prompt('Enter password') !== this[password]) {
      throw new Error("Wrong password!")
    }
  }

  return Company
})()
