const yojjiCompany = new Company('Yojji', 4)
const yojjiAdmin = Company.createAdmin(yojjiCompany)
const section = document.createElement('section')

section.innerHTML += `
  <div class="d-flex justify-content-between align-items-center">
    <h2 class="company-title">${yojjiCompany.name}</h2>
    <h2>Max users: ${yojjiCompany.usersLimit}</h2>
    <h2 class="users-count">Users: ${yojjiCompany.usersCount}</h2>
  </div>
  <table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Lastname</th>
        <th scope="col">role</th>
        <th scope="col">id</th>
      </tr>
    </thead>
    <tbody class="company-users" data-company="${yojjiAdmin.id}">
    </tbody>
  </table>
`

document.body.insertAdjacentElement('afterbegin', section)

yojjiCompany.registerUserCreateCallback(user => {
  const usersBlock = document.querySelector(`tbody[data-company="${yojjiAdmin.id}"]`)
  const usersCount = document.querySelector(`.users-count`)

  usersCount.innerText = `Users: ${yojjiCompany.usersCount}`
  usersBlock.innerHTML += userTemplate(user, yojjiCompany.usersCount - 1)
  notify(`User with id ${user.id} was created`)
})

yojjiCompany.registerUserUpdateCallback(data => {
  if (typeof data === 'number') {
    notify(`User with id ${data} was deleted`)
    return
  }

  const userBlock = document.querySelector(`tr[data-id="${data.id}"]`)
  const userCount = userBlock.firstElementChild.textContent

  userBlock.innerHTML = userTemplate(data, userCount)
})

yojjiCompany.registerNoSpaceNotifier(message => {
  notify(message)
})

function userTemplate(user, userCount) {
  return `
    <tr data-id="${user.id}">
      <th class="user-count" scope="row">${userCount}</th>
      <td>
        <span class="p-2">${user.name}</span>
      </td>
      <td>
        <span class="p-2">${user.lastName}</span>
      </td>
      <td>
        <span class="p-2">${user.role || ''}</span>
      </td>
      <td>
        <span class="p-2">${user.id}</span>
      </td>
      <td>
        <button type="button" class="close" aria-label="Close">
          <span class="close" aria-hidden="true">×</span>
        </button>
      </td>
    </tr>
  `
}

function notify(message) {
  const toast = document.querySelector('.toast')
  const toastText = document.querySelector('.toast-body')

  toast.style.opacity = 1
  toastText.innerText = message
  setTimeout(() => toast.style.opacity = 0, 2000)
}

section.addEventListener('click', (e) => {
  if (e.target.className !== 'close') return
  const user = e.target.closest('tr')
  const usersCount = document.querySelector('.users-count')

  yojjiAdmin.deleteUser(+user.dataset.id)
  user.remove()

  const usersNumberBLock = document.querySelectorAll('.user-count')
  usersCount.innerText = `Users: ${yojjiCompany.usersCount}`
  usersNumberBLock.forEach((block, index) => block.innerText = index + 1)
})

const mark = yojjiAdmin.createUser('Mark', 'Zuckerberg')
const alex = yojjiAdmin.createUser('Alex', 'Dang')
